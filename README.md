# Symfony 5 Docker Starter pack

## Introduction

This starter pack alpine php-fpm with a Symfony 5 project and nodeJs, yarn, phpmyadmin and maildev.

Using Docker and docker-compose to build it up.

## Installation
Change the .env file as whatever you want it to look like.

Run a docker-compose build:
```
 docker-compose up --build -d
```
Connect to the container:
```
 docker-compose exec app ash
```
Once you're in the container run a composer install and yarn install.
```
composer install
```
```
yarn install
```

Then do whatever you want, it's your project now buddy!